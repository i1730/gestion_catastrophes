export interface Solution{
  id: number,
  nomCatastrophe,
  username,
  message,
  date,
}
