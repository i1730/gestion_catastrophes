export interface Users{
  id?: any,
  username: string,
  email: string,
  password: string,
  imageUser: string,
}
